delete from annotations where project = 'eclipseplatform';
LOAD DATA LOCAL INFILE 'eclipseplatform-annotations.data' INTO TABLE annotations
		FIELDS TERMINATED BY '\t' (project, module, filename, revision, annotation_container, container_granularity, annotation_property, annotation_type);
