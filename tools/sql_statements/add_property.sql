alter table parameterized_types add column property varchar(20);
alter table parameterized_types_changed add column property varchar(20);

alter table rawtypes add column property varchar(20);
alter table rawtypes_changed add column property varchar(20);
